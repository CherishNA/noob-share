# 大家分享的书籍都在这里

<br>

### 须知
* 分享的书籍请自行上传到网盘按照下面分类添加自己的链接即可
* 请大家尽量使用阿里云盘，杜绝X度云盘从我做起
* 她好我也好 爱她记得用肾宝

### 免责
* 所有数据均来自网友分享，若涉及版权问题，请联系删除。
<br>

<br>

## 
## 开发语言
> golang
>> [Go程序设计语言.pdf](https://www.aliyundrive.com/s/NrzMg9AivtQ
)

> php

>java
 
>python

<br>

## 数据库
> redis
>>[Redis开发与运维(付磊).pdf](https://www.aliyundrive.com/s/mKuzngxQMD4)

>mysql

>rabbitmq

## docker 

## k8s
>> v2ex K8S入门 https://www.v2ex.com/t/821329#reply61
>>https://k8s.easydoc.net/docs/dRiQjyTY/28366845/6GiNOzyZ/Q2gBbjyW 配套课件
>>https://www.bilibili.com/video/BV1Tg411P7EB/ 视频地址 
