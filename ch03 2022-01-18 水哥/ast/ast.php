<?php
require_once 'vendor/autoload.php';

use PhpParser\NodeDumper;
use PhpParser\ParserFactory;

$code = file_get_contents('code.php');
$parser = (new ParserFactory())->create(ParserFactory::PREFER_PHP7);

try {
    $ast = $parser->parse($code);
} catch (\Exception $e) {
    print "Parse error: {$e->getMessage()} \r\n";
    exit(1);
}

$dumper = new NodeDumper();
print $dumper->dump($ast) . "\r\n";

// kind = ZEND_AST_BINARY_OP
// add(+)、sub(-)、mul(x)、pow、div(/)、mod(%)、shift_left(<<)、shift_right(<<)
// concat(.)、identical(===)、not_identical(!==)、equal(==)、not_equal(!==)
// smaller(<、>)、smaller_or_equal(<=、>=)

// 在深入 zend vm 的操作
// php-src\Zend\zend_execute_API.c@zval_update_constant_ex
// php-src\Zend\zend_ast.c@zend_ast_evaluate
// php-src\Zend\zend_opcode.c@get_binary_op
// php-src\Zend\zend_operators.c@is_equal_function
// php-src\Zend\zend_operators.c@compare_function
