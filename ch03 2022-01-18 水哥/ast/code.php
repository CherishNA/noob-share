<?php
// Expr_Assign Expr_Variable Expr_ConstFetch(null可能是官方封装常量)
$a = null;
// Expr_Assign Expr_Variable Scalar_LNumber
$b = 0;
// NotEqual
print $a != $b;
// Equal
print $a == $b;
// NotIdentical
print $a !== $b;
// Identical
print $a === $b;
// Expr_Print Scalar_String
print "hello world";
