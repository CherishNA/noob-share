package RWMutex

import (
	"fmt"
	"noob-share/ch04_2022-01-25_键盘哥/goSync/model"
	"testing"
	"time"
)

func TestRWMutex(t *testing.T) {

	var countRw model.CounterRW

	for i := 0; i < 10; i++ { //是个reader 一直读count
		go func() {
			for {
				countRw.Count()
				time.Sleep(time.Millisecond) //每毫秒查询一次

			}
		}()
	}

	for { //一个writer 持续写count
		countRw.Incr()
		fmt.Println("写入count了")
		time.Sleep(time.Second) //相当于每秒写一次， 竞争所的频次低
	}
}
