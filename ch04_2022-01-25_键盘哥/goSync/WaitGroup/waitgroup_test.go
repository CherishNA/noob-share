package WaitGroup

import (
	"fmt"
	"noob-share/ch04_2022-01-25_键盘哥/goSync/model"
	"sync"
	"testing"
	"time"
)

func wocker(c *model.Counter, wg *sync.WaitGroup) {
	defer wg.Done()
	time.Sleep(time.Second) //睡一会 +1s蛤
	c.Incr()                //给计数器加一
}

func TestWaitGroup(t *testing.T) {
	var counter model.Counter
	var wg sync.WaitGroup

	wg.Add(10) //  初始值为0  将wg的计数器值设置为10 编排10个goroutine去执行任务
	for i := 0; i < 10; i++ {
		go wocker(&counter, &wg)
	}
	//检查点  阻塞等待  等待所有的goroutine都完成任务
	wg.Wait()

	//输出当前计数器的值
	fmt.Printf("当前计数器的值：%d", counter.Count())
}
