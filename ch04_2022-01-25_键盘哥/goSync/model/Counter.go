package model

import "sync"

type Counter struct {
	mu    sync.Mutex
	count uint64
}

func (c *Counter) Incr() {
	c.mu.Lock()
	c.count++
	c.mu.Unlock()

}
func (c Counter) Count() uint64 {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.count
}
//


// CounterRW RWMutex 计数器
type CounterRW struct {
	mu    sync.RWMutex
	count uint64
}

// Incr 写方法  使用写锁保护
func (c *CounterRW) Incr() {
	c.mu.Lock()
	c.count++
	c.mu.Unlock()
}

// Count 读方法 使用读锁保护
func (c *CounterRW) Count() uint64 {
	c.mu.RLock()
	defer c.mu.RUnlock()
	return c.count
}

