package mutex

import (
	"fmt"
	"noob-share/ch04_2022-01-25_键盘哥/goSync/model"
	"sync"
	"testing"
)

func TestDemo1(t *testing.T) {
	//Mutex就提供了两个方法  Lock和Unlock，
	m := sync.Mutex{}
	//进入临界区之前调用Lock方法，
	m.Lock()
	//退出临界区的时候调用Unlock方法
	m.Unlock()
}

//unLock
func TestDemo2(t *testing.T) {

	var count = 0

	var wg sync.WaitGroup //任务编排并发原语

	wg.Add(10)

	//开启10个goroutine对count++ 每个goroutine里面对count++10000次
	for i := 0; i < 10; i++ {
		go func() {
			defer wg.Done()
			for j := 0; j < 10000; j++ {
				count++
			}
		}()
	}
	wg.Wait() //等待10个goroutine完成
	fmt.Printf("count is %d\n", count)
}

//lock
func TestDemo3(t *testing.T) {

	var mu sync.Mutex //互斥锁
	var count = 0
	wgCount := 10
	var wg sync.WaitGroup
	wg.Add(wgCount)
	for i := 0; i < wgCount; i++ {
		go func() {
			defer wg.Done()
			for j := 0; j < 10000; j++ {
				mu.Lock() //我们在临界区前加锁
				count++
				defer mu.Unlock() //我们在退出临界区后解锁
			}
		}()
	}
	wg.Wait()
	fmt.Printf("count is %d\n", count)
}

//嵌入结构体
func TestDemo4(t *testing.T) {

	wgCount := 10
	var c model.Counter
	var wg sync.WaitGroup

	wg.Add(wgCount)
	for i := 0; i < wgCount; i++ {
		go func() {
			defer wg.Done()
			for j := 0; j < 10000; j++ {
				c.Incr()
			}
		}()
	}
	wg.Wait()
	fmt.Printf("count is :%d\n", c.Count())
}
