package syncpool

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
	"testing"
)

var numClacsCreated int32

//创建实例的函数
func createBuffer() interface{} {
	//特别注意这里必须要使用 原子加 不然有并发问题
	atomic.AddInt32(&numClacsCreated, 1)
	//numClacsCreated++
	buffer := make([]byte, 1024)
	return &buffer
}

func TestSyncPoolDemo2(t *testing.T) {
	runtime.GOMAXPROCS(12) //设置逻辑处理器个数也就是gmp中 p的个数 4核8G就是 8个逻辑处理器  这里为了测试核心数和处理速度以及sync.Pool的关系  核心数越多处理速度越快，需要pool里面的实例就越多。
	//创建实例 这里New接收的是 我们上面写的createBuffer这个方法
	bufferpool := &sync.Pool{New: createBuffer}

	//多个goroutine并发测试
	numWorkers := 1024 * 1024
	var wg sync.WaitGroup
	wg.Add(numWorkers)
	for i := 0; i < numWorkers; i++ {
		go func() {
			defer wg.Done()

			//申请一个buffer实例
			buffer := bufferpool.Get()
			//buffer := createBuffer()
			_ = buffer.(*[]byte)
			//fmt.Println(i2)
			//释放一个buffer实例
			defer bufferpool.Put(buffer)
		}()
	}
	wg.Wait()
	fmt.Printf("%d buffer objects were created.\n", numClacsCreated)
}
